/*create a class: template/blueprint of objects
with properties and behaviors*/
class Tofu {
   constructor()
   { //initalize the objects
   this.speed = floor(random(6, 3));
   this.pos = new createVector(width+5, random(6, height/2.5)); // Bottom of screen is for text etc.
   this.size = floor(random(25, 25));
   }
 move() {  //moving behaviors
   //this.pos.x-=this.speed;
  this.pos.x = this.pos.x - this.speed;
 }
 show() { //show as a circle
   push()
   translate(this.pos.x, this.pos.y);
   rotate(this.tofu_rotate);
   noStroke();
   fill(255,135,32);//shadow
   circle(10,20, this.size, this.size, 25);
   fill(82,229,255); //front plane
   circle(40, 30, this.size, this.size);
   pop();
}
}
