## MiniX5 

Click [here](https://sofilu.gitlab.io/aesthetic-p/miniX5/) to see my program :)

And if you want to see my source code you can click [here](https://gitlab.com/Sofilu/aesthetic-p/-/blob/master/miniX5/sketch.js) <3

![Semantic description of image](Skærmbillede_2021-03-14_kl._20.35.53.png)

_I made a little collage with the different squares my program generates_

**What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**

Rules in my program:

1.	 When one square is drawn another three squares has to be drawn inside of that square.
2.	When the color green is present in one of the squares, they change into circles.  

I think coming up with the rules is the easy part and then being able to execute them was the more difficult part. And actually, I had another rule but I couldn’t realize it in my code, so I had to change it. For the process of programming, I think that it’s really important that you start conceptualizing the rules for your program at the beginning so you don’t make them up along the way, as these set of rules can be really helpful to set some constrains for yourself and your process. My program is heavily inspired by the German painter, Josef Albers and his theories of color. I love how this very simple and graphic design can demonstrate the interaction between colors so beautifully. My program doesn’t do that much and doesn’t shift in its performance over time but can be quite memorizing to watch and see how different colors interact and sometimes creates the illusions of the squares changing in size.

**What role do rules and processes have in your work?**

My rules play a part for the auto-generative perspective in my program. You, as user, have no control over how the program unfolds and just have to observe it and let it do its thing. Which I find is really the essential thing behind auto-generative art. My program doesn’t really evolve over time but can be seen as starting over and making something new again and again, unlike the program _Langton’s Ant_ by Chris Langton which uses the rules to evolve and simulate organic complex behaviors in a set of simple rules over time.  In my program I see the rules as some sort of guideline for the how the program will execute and setup and then I created something to happen within these rules. This relates a lot to the notion of randomness in generative art and my second rule depends heavily on this aspect. The different colors that appear in my squares are created with the random syntax. Given that I chose a quite specific color with three different parameters (all random) for the second rule to be true there is a small chance that this color will ever be present in one of the squares. I think it’s really interesting to think about how probability and cause and effect can play a part in your code. So my second rule might never actually happen in my program which can seem quite contracting when it is a rule. 

**Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)?**

I really find it interesting how auto-generativity and randomness can play a part of programming. It’s a difficult topic to put into perspective and maybe I think that my miniX could present it in a better way or at least in a different way. If I could have changed anything, I would have had my focus on how generative art can be effective to break down and to put focus on political, heteronormative and racial structures in our society. Another thing that my miniX could be helpful to think about and understand is pseudo randomness and how it is influencing my program. Because how random is my program actually and how is this affecting the outcome. It is kind of paradoxical to think about how rules, that are typically related to something strict and determined, can be a generator for randomness. The conceptual artist, Sol Le Witt demonstrates this through his work _Wall Drawing_ that consist of three simple instructions which is up to interpretation by the individual how these rules are being executed. 

Another aspect of this is “can a machine originate anything? On one hand I highly believe that we are dependent on each other and one could not exist but on the other hand I actually believe that it is the computer’s ability to visualize the programmer’s ideas. By following the programmer’s instructions, the computer interpret these instructions in its own way and therefor I see the computer as important as the programmer and maybe even more. This quote from the article, _Ten questions concerning generative computer art_, that describes this relationship between computer and artist pretty good. 

_“As computers have developed, we have seen our relationship with them change and the computer’s role shift from that of a “tool” under the direct control of the artist to that of a collaborator or creative partner and, potentially, an autonomously creative entity.”_
                                                                                                                            (McCormack et al.) 

Sorry my thoughts are a bit all over the place <3 

## Reference list
- Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142

- Jon, McCormack et al. “Ten Questions Concerning Generative Computer Art.” Leonardo 47, no. 2, 2014: 135–141.
