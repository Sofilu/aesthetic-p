
class PrincessPeach {
  constructor() {
  this.x = 50;
  this.y = height - 340;
  this.vy = 0; //speed on the y-axis (velocity)
  this.gravity = 2; //makes her come down when she jumps
  this.w = 50;
  this.h = 85;
  }

  jump() {
    if (this.y == height - 340) {
    this.vy = -28;
  }
}

hits(fireball){
  return collideRectRect(this.x, this.y, this.w, this.h, fireball.x, fireball.y, fireball.r, fireball.r);
}

move() {
  this.y += this.vy;
  this.vy += this.gravity;
  this.y = constrain(this.y, 0, height - 340);
}

  show() {
    image(pImg,this.x, this.y, this.w, this.h);
}
}
