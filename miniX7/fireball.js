
class Fireball {

  constructor() {
    this.x = 750;
    this.y = height - 340;
    this.speed = 6;
    this.r = 50;
    }

move() {
  this.x -= this.speed; // speed of the fireball
}

  show() {
    image(fImg, this.x, this.y, this.r, this.r);
}
}
