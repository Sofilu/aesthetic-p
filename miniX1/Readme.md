## Read me 

Click here to see my project :) --> https://sofilu.gitlab.io/aesthetic-p/miniX1/

This is a link to my repository --> https://gitlab.com/Sofilu/aesthetic-p/-/tree/master/miniX1


![Semantic description of image](img.png)

**What have I produced?**

I have produced an ellipse that follow the mouse around and draws the same color as the ellipse. If the mouse is pressed the ellipse turns yellow, if any random key is pressed then it turns green and if nothing is pressed the ellipse is pink. For my work i was inspired by p5.js's "get started page" where you learn how to make a program start drawing circles at the positions of the mouse. So I modified the code and made it my own. I tried to make it more complex and wanted to change the shape of the mouse to a rectangle. I also tried adding more colors to change between by clicking the mouse, but I realised that it was way to difficult for me so it ended up being quite simple. 

**How would you describe your first independent coding experience (in relation to
thinking, reading, copying, modifying, writing code, and so on)?**

It was my first time coding and I don't have any experience at all, but I still found the experience quite fun and exciting. Before hand I thought that it would be more diffucult to code but Javascript seems not that far away from how we normally write, therefor it makes a lot of sense and you can work kind of intuitively. A factor that i think had a great influence on how my first coding experience turned out was p5.js. It was a great place to start and had a very easy introduction to it with great videos and a simple layout. The library p5.js has with reffereneces is especially nice to have. Although it was a great experience I can't lie.. it was also kind of frustrating at times. When you think that something will work but the you just get a blank screen with nothing on it, then it's really rewarding when you fix the problem in the code and it works. A thing when coding, that you have to have in mind is how easy it is to just find some code and then copy and paste it. But i kept my selfcontrole because you don't learn anything by copying others work. 

**How is the coding process different from, or similar to, reading and writing text?**

What i think is very similar from writing and reading to coding is the learning process. The more you learn and the more experience you get, the more comlicated and complex codes can you make. The same goes for words. New readers read slow and only knows simple words a more experienced reader can read faster and now have a bigger vocabulary. 

**What does code and programming mean to you, and how does the assigned
reading help you to further reflect on these terms?**

Coding and programming to me is a way of expressing yourself. And I definetly think that it's so much more than just a relation between man and machine. Coding is having a great influence on the world we live ind and the programmers should be aware of the power they have in shaping the world as we know it. Another aspect of coding that I started to reflect on because of the further reading was the importance of coding history and literacy. I really like how coders and programmers and the founders of p5.js are trying to give empowerment to minority groups and being as inclusive as they can. 

**Reference list**
- McCarthy, Lauren Lee. u.d. p5.js. Visited at 4th February 2021. https://p5js.org/get-started/.
