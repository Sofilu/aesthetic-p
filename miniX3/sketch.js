//arrays
let hearts = ['💖','💕 ','💘','❣️','💛','💗','❤️','💜','💔','💚','💓','💙','🧡','🖤'];


function setup () {
  createCanvas(windowWidth, windowHeight);
  frameRate(10);
}

function draw() {
  background(255, 190, 255, 80);
  drawElements(); //my function
}

function drawElements() {
  let rand = floor(random(hearts.length));//picks a random heart from the array to draw in the circle
  let allHearts = hearts[rand];

  let num=13;// number of hearts in the circle
  push(); // so the text don't rotate with the circle of hearts
  translate(width/2, height/2);// moving the circle to the center
  let cir = 360/num*(frameCount%num); // how many degrees the circle is rotating by each frame.
  rotate(radians(cir));
  textSize(25);
  text(allHearts,40,40);
  pop();// togehter with push, isolates this part to the circle

fill(250, 154, 33);// text line
noStroke();
textStyle(BOLD);
textFont("monospace");
text('Wating for him to text back..',windowWidth/2.4, windowHeight/1.5);


  function windowResized() { // so you can resize the canvas everytime you reload it
    resizeCanvas(windowWidth, windowHeight);
  }

}
