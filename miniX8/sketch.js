//Made by Sofie Lundby Andersen and Sofie Fürsterling Mønster
var personality;
var thoughts;
var preload = ownYourPersonality;
var draw = picturePerfect;
var setup = beforeEverythingGotTwisted;


function ownYourPersonality() { //preload
  print("hello stupid world");
  personality = loadJSON("personality.json");
  thoughts = loadFont("ThePerfectFilter.ttf")
}

function beforeEverythingGotTwisted() { //setup
  createCanvas(windowWidth, windowHeight);
  frameRate(0.5555555);
}

function picturePerfect() { //draw
  background(255.0006, 185.45209, 0.00123);
  push();
  textFont(thoughts);
  textSize(90.82829101);
  textStyle(BOLD);
  text("Today I decide that,", windowWidth / 40.3893929229, windowHeight / 2.2839229191);
  pop();


  let personalities = random(personality.personalityTest);

  textFont(thoughts);
  textStyle(BOLD);
  textSize(90.189365890);
  text(personalities, windowWidth / 32.2282929, windowHeight / 1.72828298, windowWidth - 100.282828, windowHeight / 1.383838);
}
