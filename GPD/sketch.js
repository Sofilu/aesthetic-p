
let weaponSize = {
  w:100,
  h:100
};
let weapon;
let pacPosY;
let mini_height;
let min_tofu = 3;  //min tofu on the screen
let tofu = [];
let score =0, lose = 0;
let keyColor = 45;
let bImg; //backgroundImage

function preload(){
  weapon = loadImage("weapon.png");
  bImg = loadImage("background.png");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  pacPosY = height/2;
  mini_height = height/2;
}
function draw() {
  background(bImg);
  fill(keyColor, 255);
  rect(0, height/1.5, width, 1);
  displayScore();
  checkTofuNum(); //available tofu
  showTofu();
  image(weapon, 0, pacPosY, weaponSize.w, weaponSize.h);
  checkEating(); //scoring
  checkResult();
}

function checkTofuNum() {
  if (tofu.length < min_tofu) {
    tofu.push(new Tofu());
  }
}

function showTofu(){
  for (let i = 0; i <tofu.length; i++) {
    tofu[i].move();
    tofu[i].show();
  }
}

function checkEating() {
  //calculate the distance between each tofu
  for (let i = 0; i < tofu.length; i++) {
    let d = int(
      dist(weaponSize.w/2, pacPosY+weaponSize.h/2,
        tofu[i].pos.x, tofu[i].pos.y)
      );
    if (d < weaponSize.w/2.5) { //close enough as if eating the tofu
      score++;
      tofu.splice(i,1);
    }else if (tofu[i].pos.x < 3) { //pacman missed the tofu
      lose++;
      //tofu.splice(i,1);
    }
  }
}

function displayScore() {
    fill(255,255,255);
    textSize(25);
    text('You have protected '+ score + " person(s) persondal data", 10, height/1.4);
    text('You have wasted your change to protect ' + lose + " person(s) persondal data", 10, height/1.4+20);
    fill(255,255,255);
    text('PRESS the ARROW UP & DOWN to help protect personal data!!',
    650, height/20);
}

function checkResult() {
  if (lose > score && lose > 2) {
    fill(255,0,0);
    textSize(50);
    text("You couldn't safe all the data... PLEASE try again", width/8, height/2);
    noLoop();
  }
}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    pacPosY-=50;
  } else if (keyCode === DOWN_ARROW) {
    pacPosY+=50;
  }
  //reset if the pacman moves out of range
  if (pacPosY > mini_height) {
    pacPosY = mini_height;
  } else if (pacPosY < 0 - weaponSize.w/2) {
    pacPosY = 0;
  }
}
