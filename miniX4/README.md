## MiniX4 - Surveillance Capitalism 

This project explores how data is collected and used by big corporations in behavioral models to predict the user’s “future”. Your mouse captures data whether you like it or not, so depending on where your mouse is on the screen different ads pop up. These ads are targeted specific for you and specified on your mood and feelings. It also presents how far we are willing to go and how much data and privacy we are ready to give up for having the ability to click on a button (Soon og Cox). 

Click [here](https://sofilu.gitlab.io/aesthetic-p/miniX4/) to see my project :)<3

![Semantic description of image](Skærmbillede_2021-03-07_kl._21.43.29.png)

**What you have used and learnt** 

For this week I have explored the possibilities with data capturing and different DOM-elements. In my project I used the button() function and the input() function, the latter which creates a text box. I also played around with the mouseX and mouseY function, which are my main data capture element in this project. I felt that the mouse is one of the most interesting aspects of data capturing and could be used in many different ways. For this project I also learned a new syntax which is loadImage() and I was my first time using .jpg files in a project. I used this to make the pop-up ads on my canvas which I found really useful and quite fun to play around with. Something I would like to change and that I really struggled with was being the idea of the ability to only be able to click on the button if you typed in your mail address. 

**How your program and thinking address the theme of “capture all.” – What are the cultural implications of data capture?**

My program relates to the theme of “capture all” by using the data input from the users mouse and keyboard. It was totally new for me that corporations use so many different types of data capturing that I really wanted it to be shown explicitly through in my work. The user is quite aware that different ads are popping up around the screen depending on where the mouse is. Also the fact the the heading says “please type in your e-mail.” Makes it quite noticeable that you have to give something up to get the satisfaction of pressing the button and learning about your future. This is also what the core text book by Winnie Soon and Geoff Cox discusses by relating on Søren Polds argument.

_“A button is “seductive,” with its immediate feedback and instantaneous gratification. It compels you to press it. “_ (Soon og Cox)

The button calls for interaction and we blindly accept this. Unlike my program other platforms such as facebook uses like buttons and emojicons as an interactive element in their software. By using these interactions and not really paying attention to what we accept, we give the permission for data capturing our lives, thoughts and experiences. Thereby our lives become datafied and used to pattern out human behaviors. This is relating to the term “surveillance capitalism” by Shoshana Zuboff which exactly describes how data is being used in a capitalistic context with the core purpose of profit-making. Companies use these data to target the consumers more precisely with ads which inspired me to implementing this aspect in my program. You have no control over these targeted ads and therefore this data capturing is a threat to democracy, liberty and freedom (Zuboff). This is also seen in my program where it uses your e-mail address to “predict” your “future”. It is somewhat quite scary to think about how much power and influence we are giving these companies and how they use it to control and maybe even change our behavior. It may be quite absurd to think that any person or even for that matter a COMPUTER! has the ability to predict your future, but nowadays it might actually be true. 

**Reference list**
- Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119
- Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary” https://youtu.be/hIXhnWUmMvw.


