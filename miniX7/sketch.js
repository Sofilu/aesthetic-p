//Welcome to the source code <3 made by Sofie

let princesspeach;
let fireballs = [];

let bImg; //background
let fImg; //fireball
let pImg; //princess peach
let peachVoice;

function preload() { //loading all my images and sound
  bImg = loadImage("bowserscastle.jpg");
  fImg = loadImage("fireball.png");
  pImg = loadImage("ppeach.png");
  peachVoice = loadSound('voice.mp3');
  bowserVoice = loadSound('bowser.mp3');
  myFont = loadFont('8bitOperatorPlusSC-Bold.ttf');

}

function setup() {
  createCanvas(1125, 723);
  print("hello world");
  princesspeach = new PrincessPeach();
}

function keyPressed() { //princess peach's movements
  if (keyCode === 32) {
    princesspeach.jump();
      peachVoice.play();
  } else if (keyCode === LEFT_ARROW) {
    princesspeach.x-=30;
  } else if (keyCode === RIGHT_ARROW) {
    princesspeach.x+=30;
  }
  if (princesspeach.x > 230) {
    princesspeach.x = 230;
  } else if (princesspeach.x < 0) {
    princesspeach.x = 0;
  }
}

function draw() {

if (random(2)<0.01) { //makes bowser spit random fireballs
  fireballs.push(new Fireball());
}
background(bImg);
princesspeach.show();
princesspeach.move();

push();
fill(255);
textAlign(CENTER);
textFont(myFont);
text('Use the left and right arrow keys and spacebar', 455, 550);
text('to move peach and avoid bowsers´s fireballs', 445, 570)
//text('Move Peach from side to side and jump to avoid Bowsers fireballs',10,700);
pop();

for (let i = 0; i <fireballs.length; i++) {
    fireballs[i].move();
    fireballs[i].show();

if (princesspeach.hits(fireballs[i])) {
      console.log('game over');
      fill(255);
      textSize(30);
      textFont(myFont);
      text('GAME OVER', 900/2,723/2);
      bowserVoice.play();
      noLoop();

 }
}
}
