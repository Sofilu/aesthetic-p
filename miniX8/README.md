## MiniX8 - Coding new personality 

This readMe and runMe is made by Sofie Lundby Andersen and [Sofie Fürsterling Mønster](https://gitlab.com/Sofiefm)

[RUNME](https://sofilu.gitlab.io/aesthetic-p/miniX8/)

[REPOSITORY](https://gitlab.com/Sofilu/aesthetic-p/-/tree/master/miniX8)

![Semantic description of image](Skærmbillede_2021-04-09_kl._14.50.08.png)

**Description of our artwork**

“Coding new personality” is an art piece that explores the notion of how source code and critical writing operates together in the making of personality poems. By this piece we wanted to challenge the personality standards of today’s society in code and language and maybe being able to set new standards for, or challenge social media’s view on “the perfect person”. Different personality types complete the phrase “Today I decide that,..”. This phrase is a way of giving personal empowerment to the user and giving them a choice of their own to be who they are. “Coding new personality” seems perfect on the outside with a clean graphical interface, this is not the case with the source code. Behind the scene lies hidden messages and ugly numbers waiting to be explored.   

**Inspiration and reasons for our design process**

The purpose with this mini exercise was to learn and understand how the technical part of JSON works and at the same time reflect upon the aesthetics of code and language. We had to think about text and how it can take various forms such as code, which is visualized in our work. We found our inspiration from this interesting work: http://amiraha.com/mexicansincanada/ made by Amira Hanafi. We really liked the simplicity of the piece, because even though it is very simple, we still got curious about it, and kept on trying to read the different text paragraphs. Actually we find it a bit frustrating not being able to see, understand, and reflect about the end of the sentence, although there is certainly a greater conceptual reason for it… We quickly agreed that in our work the viewer should be able to finish the sentence. What we also liked about Hanafi’s work is how it appeared to us as relatable and understandable even though we weren’t able to read the words, it still made us feel something, which probably depended on the first steady sentence, where the words are never going to change. We wanted to incorporate this in our final result, because we think it has a visual and empowering effect, it makes those few words seem really important. 

We ended up creating an e-lit poem, but it is important to mention that how to interpret or relate to it is impossible to do wrong. It depends on the person reading it whether it should be understood as; a funny feature, something with a deeper message, something motivating, something to make them feel empowered or something else. 

**Description of our program** 

Since the use of JSON files are new to both of us, we decided to create something simple, and then maybe evolve on it (depending on how much time we got). The main focus for our program was to make the text change at a lower speed so the viewer was able to read some of the poems. The different texts from the JSON file appear random on the canvas which we see as a way of describing how things “randomly” happen in life and how we change because of these happenings. We developed our conceptual message while writing the code, and we enjoyed creating vocable code by changing the name of the functions and other elements in the sketch.js file. For example we chose to name our draw function “picturePerfect” to symbolize that it often looks like people are perfect on the outside and when encountering new people that is all you see at first. Our source code doesn’t look like the normal way of coding in p5.js where the names of the functions and variables resemble normal language; this is what Wendy Chun refers to as “sorcery”. We changed the normal functions names and adds on to the this sorcery where the machine code becomes more and more hidden. 

**Reflection upon the subject Vocable Code**

The title of this week's chapter in Aesthetic Programming was “Vocable code” the benefits or use of this is described in the following quote: 

>_“...the ways in which the voice of the human subject is implicated in coding practices, and how coding itself can “voice” wider political issues…”_ (Cox, Soon, Aesthetic Programming, p. 167). 

This quote really inspired us for this week's exercise, usually we have more focus on the outcome (runMe), but after reading this chapter we started to look upon code in a new perspective and therefore put a lot of thoughts in naming different elements in our code.

We wanted to focus on identity and personal empowerment and therefore we chose the unchangeable text to be “Today I decide that, …”, this text phrase puts the person reading it in a position where they need to take control. By using a JSON file with a large array we hope that there will be at least one sentence that each viewer can relate to or just find funny. The message with this program is that people should own their actions, and don’t get bothered by the norms and expectations of the society surrounding them. We also wanted to reflect upon how your personality isn’t a constant thing but a thing that changes and evolves over time, this is symbolized through the way that the text constantly changes and never stops at just one. The younger generation is especially affected by the pressure of looking flawless on social media, and we really wanted to highlight that problem in our work. When looking at our runMe everything looks nice, and without any flaws, but when reading the code in our sketch.js file you will realize what the work is really about — nothing is as simple and perfect as it looks like. 

**References**

- Inspiration: http://amiraha.com/mexicansincanada/

- Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 167-186

- JSON-file: https://github.com/dariusk/corpora/tree/master/data/psychology

- Font: https://github.com/theleagueof/ostrich-sans , https://www.theleagueofmoveabletype.com/ostrich-sans

