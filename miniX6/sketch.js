let = death;
let = women;
let = button;

function preload() { //loaning my pictures
  death = loadImage("death.png");
  women = loadImage("women.png");
}


function setup() {
  // put setup code here
  createCanvas(windowWidth,windowHeight);
  print("hello world");

  resetSketch();

  let col = color(0,0,0);

  button = createButton('Text me when you get home') //button
  button.position(width/3, height/2);
  button.mousePressed(resetSketch);
  button.style('background',col);
  button.style('font-size', '20px');
  button.style("color", "#fff");
  button.style("font-family", "monospace");
}

function resetSketch() { //reset sketch function
    createCanvas(windowWidth,windowHeight);
  }

function draw() {
    if (mouseIsPressed){
      image(death, mouseX-15, mouseY-15); //death image is used to draw with
    } else {
      image(women, mouseX-15, mouseY-15); //women image is used to draw with
    }
}

function windowResized() { // so you can resize the canvas everytime you reload it
  resizeCanvas(windowWidth, windowHeight);
}
