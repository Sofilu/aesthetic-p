# MiniX 6 – Society 2.0 

 [RUNME](https://sofilu.gitlab.io/aesthetic-p/miniX6/)	

 [MY REPOSITORY](https://gitlab.com/Sofilu/aesthetic-p/-/tree/master/miniX6)

**REWORK AN OLD MINIX** 

For this week I decided to revisit my first miniX. The reason for this is first of all this was my first miniX and I didn’t have any intention or critical making in the design process. This was just an exercise for me to explore P5.js and starting to get familiar with programming. This time I wanted to strengthen the conceptual thinking behind my program so, I thought that this would be a nice miniX to rework on. 

![Semantic description of image](Skærmbillede_2021-03-21_kl._21.29.56.png)*miniX1* 

![Semantic description of image](Skærmbillede_2021-03-21_kl._21.12.22.png)*miniX6*



I wanted to have the same basic elements as my original miniX so it would be a nice demonstration of how I have used aesthetic programming in my new miniX. I used new syntaxes that I’ve learned over the past couple of weeks e.g. loadImage() and createButton(). LoadImage is used to being able to draw with a preloaded picture instead of drawing with a circle that changes in color. I also made my own functions to reset the canvas when the button is clicked. The inspiration for my miniX this week came from the recent kidnapping and death of Sarah Everard which sparked a debate about women being afraid and not feeling safe in public spaces because of the violence and abuse carried out by men. The Greek goddess-like statue symbolizes women and the orange flower, which is a chrysanthemum, symbolizes death. I created a button at the center of the canvas that says “text me when you get home” which is the hashtag used in the debate. When clicking this button everything disappears and you will be able to draw something new. I see this button as a representation of wanting to delete and change everything with one simple sentence, which in fact can’t help anything because all the death and violence will be able to continue anyway. 
This whole thinking and research process I had before making my program I believe is a central idea of aesthetic programming. In critical making the focus is not on the end result or outcome, but how the design process has contributed to the final product and how it relates to the world we live in. As many other artforms programming can be used as a medium for critical thinking, expressing and shedding light on fundamental problems in today’s society, which I see as an important skill for designer’s these days to have.  


 **AESTHETIC PROGRAMMING & CRITICAL MAKING** 


Through working with my miniX6 It has helped me to reflect upon how programming is used as a practice and also as a design method. I really like this quote from the aesthetic programming handbook, which refers to how programming is used as a way of thinking about the world. 



_”We therefore consider programming to be a dynamic cultural practice and phenomenon, a way of thinking and doing in the world, and a means of understanding some of the complex procedures that underwrite and constitute our lived realities, in order to act upon those realities.” (Soon & Cox, 14)_



As they also mention further on, programming can be both considered as a practice to build things, and make world, but also seen as a way of drawing critique upon technology, art and cultural theory (Soon & Cox). Therefor I see programming as one of many methods in the fields of design that encourages reflection, participation and critiquing. 
Since we live in a continuously more and more technological and digital world, I definitely see aesthetic programming as an important tool for not only getting a practical understanding of how everything works behind the scenes, but also for giving the opportunity to open up often hidden layers, practices and values in our society and culture. And I think it is important to understand the dynamics behind this and learn that everything is not neutral and without any intention. 


Often when I’m programming, I always feel like making something complex, which quite often fails because of my programming skills. So, for me it it’s quite important that we have the readMe to be able to reflect upon certain things in our program. But it’s not only for me the readMe is important I also see this aspect of reflecting upon your program quite important for the idea of aesthetic programming and critical making. Even if my program is bad or good it helps me to reflect and think critically on programming in a cultural sense. 

**REFERENCE LIST**
- Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24


