## MiniX7 – Super Peach Sisters 

[RUNME](https://sofilu.gitlab.io/aesthetic-p/miniX7/)

[REPOSITORY](https://gitlab.com/Sofilu/aesthetic-p/-/tree/master/miniX7)

Play it in Safari or Chrome 

![Semantic description of image](Skærmbillede_2021-03-29_kl._14.19.23.png)

**Describe how does/do your game/game objects work?**

For this week I wanted to make a simple an easy game. One of the first games that I encountered when I was a kid, and have played ever since, is ´Super Mario Bros´. I played it every day and every night with my little sister on our dad’s old super Nintendo from the 90’s. This game has a special place in my heart, and I thought It would be interesting to have it as inspiration for this week with object-oriented programming. Mine is not even that complex and is built on two objects interacting with each other. You play the game by moving Princess Peach left and right by using the arrow keys, by pressing the spacebar she jumps. You have to avoid being hit by bowser’s fireballs which he spits out randomly. You can’t win, only loose. 

As I mentioned earlier my game consists of two classes of objects: `ppeach.js` and `fireball.js`

Ppeach class has six properties: x and y positions, a velocity, gravity, and a with and a height. She has four functions: `jump();` which makes her able to jump over the fireballs, `hits();` detects when a fireball hits peach, `move();` is also related to jump and uses gravity to not make her fly away when she jumps and `show();` which makes her appear with a .png file but still has the same arguments as a rectangle. 

The fireball class has four properties: x and y position, speed, and a radius. Fireball only has two functions `move();` that makes it move towards with a certain speed and `show();` that has the same function as ppeach and uses a .png file I preloaded in my sketch.js. Fireball is used in my sketch as an array and is appearing on the canvas with a random interval.

 It is also in my sketch.js that I use `keyPressed();` that controls the ppeach object. For calculating the distance between to objects I downloaded another library to my project which contains this calculation as an already premade function. It has different functions with different objects but I used the one that uses rectangles `collideRectRect();`. You only have to put the x,y,w and h values of both of the objects you want to detect if they are hitting, in your function. 


**Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?**

Object-oriented programming can be seen as an abstraction of the real world, and it is definitely a reflection upon how we see the world. In the early days of Super Mario, peach was seen as damsel in distress and Mario was the only one who could save her from the evil turtle, Bowser. This is not only a problem in Mario, but definitely a problem in our society and culture, where fairy tales with princesses that needs to be saved by a strong prince, is a normal way of thinking which is also seen in how Disney movies depicts women. From all this I wanted to make Princess Peach the main character and protagonist of my game. She is no longer the damsel in distress but a woman who takes up the challenge to defeat Bowser.  

I know that I’m not changing anything, but I think that this way of programming really put into the perspective of how objects relate to each other not only in programming, but also in our society. 

> _"With the abstraction of relations, there is an interplay between abstract and concrete reality. The classifications abstract and concrete were taken up by Marx in his critique of capitalist working conditions to distinguish between abstract labor and living labor. Whereas abstract labor is labor-power exerted producing commodities that uphold capitalism, living labor is the capacity to work."_ (Cox & Soon, 161)

I like how this quote describes OOP as an interplay between abstract and concrete reality. In many ways our world is so difficult and complex, but we still try to fit all these complex matters and relations in to one simple program. The abstraction is being put into thinking of objects such as squares and circles with properties and abilities. I think that this I quite contradicting in many ways, because even though you have good intentions complexities from the real world will no matter what, be sorted from the context and therefore loose some of its meaning. But as this quote also states, is classes and objects an abstraction of code, and therefore it also reflects on how we can choose to look at society through OOP. 

It is also quite interesting to think about how computers think and interpret data and how we as humans do it. The computer needs some basic structures to recognize something as e.g. Princess Peach. This is so far away from how we human’s attributes properties and characteristics to a person for it to be a person. There is just so much more than just properties and functions that makes a person a person and a human being. We need personality, feelings, thoughts and so on and so on... But still, we find OOP as some sort of bridge between the computer and us and a way and a method of abstracting on a computational level but also on a culture- and society-based level. 

**Reference list**

- Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
- Peach png: https://www.seekpng.com/ipng/u2q8o0e6q8w7y3q8_princess-peach-princesa-peach-pixel-art/
- Fireball png: http://pixelartmaker.com/art/0450fadc6149a32
- Background: https://wallpapercave.com/bowsers-castle-super-mario-wallpapers
- Collide library: https://github.com/bmoren/p5.collide2D
- Sounds: https://themushroomkingdom.net/media/mk64/wav

