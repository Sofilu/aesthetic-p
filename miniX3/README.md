See my project [here](https://sofilu.gitlab.io/aesthetic-p/miniX3/) (:

![Semantic description of image](Skærmbillede_2021-02-20_kl._15.19.18.png)

## My program 

For this week it was really struggling with coming up with a concept for my project. I kept changing it again and again but ended up with this. I had so many cool ideas, but my brain couldn’t figure out how to do it in p5.js. I still have to learn that it takes time and not everything you make have to be perfect and complicated. For this week I wanted to explore some of the new functions we learnt and implement them in my project.


My concept for this week is a throbber that symbolizes how you have to wait for someone to text you back. It is made out of all the heart emojis from apple. Time can feel different from when you are expecting a text from someone you really like. It can not only feel like hours but sometimes it is actually taking hours for them to respond. I based this throbber on my own personal life and the experiences of my friends. The boy doesn’t seem to have the same concept of time and priorities as you and often unintended signals can be misinterpreted. It is crazy to think about how big an impact time and text messages can have on your relationship. Another aspect is how time can be used to send a message to someone and that time is a powerful tool to make you feel like you have control over another person. 

## Time-related syntaxes/functions that I have used in my program 

In my project I used the syntaxes: framerate(); and rotate();, which are related to how time is shown in my project. The framerate decides how fast my throbber spins and the rotate to place the hearts and rotate them.  I wanted the throbber to not move that fast so it doesn’t seem that the time is going fast but I also didn’t want it to move to slow.  Even though the time might seem to move in snail speed when you are waiting for a text. In Hans Lammerant’s text: “How humans and machines negotiate experience of time”, he discusses how technology and designs has had an effect on how we as human perceive time. This is something I find really interesting how the ability to communicate with someone far away has made us so aware and sensitive to time. Before you had to send a letter with the postal service, and it could sometimes take days and months for the other person to receive it and reply back by sending another letter. Communicating by letter was (and still is) a really long process and nowadays communication time has totally shifted to expecting a quick reply. If these expectations aren’t met it can cause a feeling of being ignored and not feeling interesting enough for the other person to respond right away. 
 

The throbber has definitely become a symbolic representation of time and it mimics our thinking of time walking/going/flying. It communicates that the computer needs time to think about a process just like we do as a human. It has just become such a normal thing that we don’t even pay attention to them anymore. 

### Reference list
- Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018)


