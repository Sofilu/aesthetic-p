let input;
let button;
let greeting;
let futures = ['You will die before you turn 30', 'Tomorrow you will adopt a homeless dog', 'You will have pizza and icecream for dinner tonight', 'You are pregnant','You will never be loved'];
let = libresse;
let = mcd;
let = trump;
let = dating;

function preload () { //the different pictures are being loaded
  libresse = loadImage("libresse.jpg");
  mcd = loadImage("mcd.jpg");
  trump = loadImage("trump.jpg");
  dating = loadImage("dating.jpg");
}

function setup() {
  createCanvas(1000,700);
  print("hello world");

//text box
  input = createInput();
  input.position(420,300);

//the submit button
  button = createButton('submit');
  button.position(590,302);


button.mousePressed(change);

//using heading h2 to make a headline
  greeting = createElement('h2','Please type your e-mail address to see your future');
  greeting.position(280,240);
}

function draw() {
  background(200);
  text('e-mail:', 378, 315);

  if (mouseX > 200) { // libresse ad
    image(libresse, -50, 350);
  }
  if (mouseX > 500) { // mcdonalds ad
   image(mcd, 600, 50);
 }
 if (mouseY > 600) { //vote for trump ad
  image(trump, 600, 400);
  }
  if (mouseX > 800) { //dating ad
    image(dating, 50, 10);
  }
}

function change() { //the text changes
  let rand = floor(random(futures.length)); //choosing a random future from the array
  let allFutures = futures[rand];

  greeting.html(allFutures);
  greeting.position(350,240);
}
